package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {
	private Dosis doser[];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double morgenAntal,
			double middagAntal, double aftenAntal, double natAntal) {
		super(startDen, slutDen, laegemiddel);
		doser = new Dosis[4];
		opretDoser(morgenAntal, middagAntal, aftenAntal, natAntal);

	}

	@Override
	public double samletDosis() {
		double counter = doegnDosis();
		int antalDage = super.antalDage();
		return counter * antalDage;
	}

	@Override
	public double doegnDosis() {
		double counter = 0;
		for (Dosis dosis : doser) {
			counter += dosis.getAntal();
		}
		return counter;
	}

	@Override
	public String getType() {
		return "Daglig Fast";
	}

	public Dosis[] getDoser() {
		return doser;
	}

	public void opretDoser(double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		LocalTime tidMorgen = LocalTime.parse("07:30");
		LocalTime tidMiddag = LocalTime.parse("12:00");
		LocalTime tidAften = LocalTime.parse("17:30");
		LocalTime tidNat = LocalTime.parse("22:30");
		Dosis dMorgen = new Dosis(tidMorgen, morgenAntal);
		Dosis dMiddag = new Dosis(tidMiddag, middagAntal);
		Dosis dAften = new Dosis(tidAften, aftenAntal);
		Dosis dNat = new Dosis(tidNat, natAntal);
		doser[0] = dMorgen;
		doser[1] = dMiddag;
		doser[2] = dAften;
		doser[3] = dNat;
	}

}