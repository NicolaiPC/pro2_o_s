package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {

	private ArrayList<Dosis> doser = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
		super(startDen, slutDen, laegemiddel);
	}

	// -------------------------------------------------------------------------------------
	/*
	 * Get, add og remove Dosis
	 */
	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(doser);
	}

	// -------------------------------------------------------------------------------------
	/*
	 * Metoder til Superklassens abstrakte metoder.
	 */

	@Override
	public double samletDosis() {
		double counter = doegnDosis();
		int antalDage = super.antalDage();
		return counter * antalDage;
	}

	@Override
	public double doegnDosis() {
		double counter = 0;
		for (Dosis dosis : doser) {
			counter += dosis.getAntal();
		}
		return counter;
	}

	@Override
	public String getType() {
		return "Daglig Skæv";
	}

	// -------------------------------------------------------------------------------------
	/*
	 * Metode til at oprette en daglig dosis og den bliver tilføjet på dosiser
	 * arrayet.
	 */
	public void opretDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		doser.add(dosis);
	}
}