package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {
	private double antalEnheder;
	private ArrayList<LocalDate> givneDoser = new ArrayList<>();

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antalEnheder) {
		super(startDen, slutDen, laegemiddel);
		this.antalEnheder = antalEnheder;
	}

	// ------------------------------------------------------------------------------

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 *
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		boolean result = false;

		if (super.getStartDen().compareTo(givesDen) > 0 && super.getSlutDen().compareTo(givesDen) < 0) {
			givDosis(givesDen);
			result = true;
		}
		return result;
	}

	@Override
	public double doegnDosis() {
		double total = 0.0;
		if (!givneDoser.isEmpty()) {
			int antalGangeGivet = givneDoser.size();
			LocalDate first = givneDoser.get(0);
			LocalDate last = givneDoser.get(givneDoser.size() - 1);
			long dage = ChronoUnit.DAYS.between(first, last);
			total = (antalGangeGivet * antalEnheder) / dage;
		}
		return total;
	}

	@Override
	public double samletDosis() {
		double total = 0.0;
		int dage = super.antalDage();
		total = (givneDoser.size() * antalEnheder) / dage;
		return total;
	}
	// ------------------------------------------------------------------------------

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 *
	 * @return
	 */
	public int getAntalGangeGivet() {
		return givneDoser.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public Laegemiddel getLaegemiddel() {
		return super.getLaegemiddel();
	}

	@Override
	public String getType() {
		return "Pro Necesare";
	}
}